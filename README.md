Hub
===

##Dev setup
Make sure you have Node, NPM, and Bower installed.  
To install the dependencies:  
`$ npm install && bower install`

To start up an Express server and watch changed files to refresh the browser on file changes:  
`$ gulp`

##Testing
We have both e2e and unit tests for Angular. Unit tests reside on the server side as well, both using Jasmine.  
Gulp tasks are:  
`$ gulp watch:e2e`  
`$ gulp watch:unit`  
`$ gulp watch:server`

[Protractor docs](https://github.com/angular/protractor)  
[Jasmine docs](http://jasmine.github.io/2.0/introduction.html)

##Components
- Node.js v0.10
- Express v4.4
- Angular v1.8
- MongoDB - mongolab



