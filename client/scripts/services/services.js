'use strict';

angular.module('sohAppServices', ['ngResource'])
	.factory('Services', ['$resource', function($resource){
		return $resource('api/services', {}, {
			query: {method:'GET', isArray: true}
		});
	}])
	.factory('Socket', ['$rootScope', function($rootScope){
		var socket = io.connect();
		return {
			on: function(event, callback){
				socket.on(event, function () {
				    var args = arguments;
				    $rootScope.$apply(function () {
				    	callback.apply(socket, args);
				    });
			    });
			},
			emit: function(event, data, callback){
				data = data || {};
				callback = callback || null;
				socket.emit(event, data, callback);
			}
		};
	}]);
