'use strict';


angular.module('sohApp')
	.controller('HomeCtrl', ['$scope', 'Socket',
		function($scope, Socket) {
			
			$scope.postsOnPage = 10;
			$scope.feed = [];

			Socket.on('feed', function(data){
				if(!data || data.error){
					alert(data.error);
				}else{
					$scope.feed = data;
				}
			});
			
			$scope.init = function(){
				$scope.getPosts();
			};
			
			$scope.getPosts = function(){
				Socket.emit('getFeed', {limit: $scope.postsOnPage});
			};

			$scope.init();
		}
	]);
	
