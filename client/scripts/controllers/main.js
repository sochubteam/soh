'use strict';


angular.module('sohAppControllers', [])
	.controller('MainCtrl', ['$scope',
		function($scope) {
			$scope.pageTitle = 'SOH';
			
			$scope.status = 'Ready';
			$scope.test = 'MainCtrl test';

		}
	]);
