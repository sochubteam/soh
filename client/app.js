'use strict';

angular.module('sohApp', [
	'sohAppServices',
	'sohAppControllers',
	'ngRoute',
	'ngResource',
	'ngCookies',
	'ngSanitize',
	'ngDialog'
])
	.config(function($routeProvider, $locationProvider) {
		$routeProvider
			.when('/', {
				templateUrl: 'views/home',
				controller: 'HomeCtrl'
			})
			.when('/welcome', {
				templateUrl: 'views/welcome',
				controller: 'AuthCtrl'
			})
			.when('/styles', {
				templateUrl: 'views/styles',
				controller: 'MainCtrl'
			})
			.otherwise({
				redirectTo: '/'
			});

		// we won't be using hashbangs
		$locationProvider.html5Mode(true);
	})
	.run(function($location){
    	console.log("Into run mode");

        //now redirect to appropriate path based on login status
        if( true ){
          //$location.path('/login');
        }
	});


