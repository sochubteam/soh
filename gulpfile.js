var gulp = require('gulp')
	, $ = require('gulp-load-plugins')({lazy: false})
	, lr = $.livereload()

// you need to have the livereload extension installed on your browser to benefit:
// http://ow.ly/xB9ke


gulp.task('lint', function() {
	gulp.src(['client/*.js','client/scripts/**/*.js'])
		.pipe($.jshint())
		.pipe($.jshint.reporter($.jshintStylish))
		.pipe($.livereload())
})

gulp.task('templates', function() {
	return gulp.src(['client/*.jade', 'client/views/*.jade'])
		.pipe($.jade({pretty: true}))
		.pipe(gulp.dest('dist/views'))
})

gulp.task('styles', function() {
	return gulp.src(['client/styles/main.scss', 'client/styles/base/*.scss'])
		.pipe($.rubySass())
		.on('error', $.notify.onError())
		.pipe($.autoprefixer('last 2 versions'))
		.pipe($.rename('main.css'))
		.pipe(gulp.dest('.tmp'))
		.pipe($.livereload())
})

gulp.task('minify', function() {
	gulp.src(['client/*.js', 'client/scripts/**/*.js'])
		.pipe($.concat('main.min.js'))
		.pipe($.uglify())
		.pipe(gulp.dest('dist/js'))

	gulp.src('client/bower_components/**/*.min.js')
		.pipe($.concat('vendor.min.js'))
		.pipe($.uglify())
		.pipe(gulp.dest('dist/js'))

	gulp.src('.tmp/main.css')
		.pipe($.minifyCss())
		.pipe($.rename('main.min.css'))
		.pipe(gulp.dest('dist/css'))

	return	gulp.src('client/images')
		.pipe($.imagemin())
		.pipe(gulp.dest('dist/img'))
})


var files = [
	'dist/views/404.html',
	'dist/views/500.html',
	'dist/views/index.html'
]
gulp.task('clean', function() {
	gulp.src(files)
		.pipe(gulp.dest('dist'))
	gulp.src(files[0])
		.pipe($.clean())
	gulp.src(files[1])
		.pipe($.clean())
	return gulp.src(files[2])
		.pipe($.clean())
})

gulp.task('replace', function() {
	return gulp.src('dist/index.html')
		.pipe($.htmlReplace({
			'css': 'css/main.min.css',
			'vendor': 'js/vendor.min.js',
			'js': 'js/main.min.js'
		}))
		.pipe(gulp.dest('dist'))
})

gulp.task('serve', function() {
	$.nodemon({script: 'server.js', ext: 'js', ignore: ['client', 'test', 'node_modules']})
		.on('restart', function() {
			console.log('server restarted!')
		})
	setTimeout(function() {
		require('opn')('http://localhost:1337')
	}, 666)
})

gulp.task('watch', function() {
	var server = $.livereload()
		, reloadPage = function(evt) {
		server.changed(evt.path)
	}
	// templates
	gulp.watch(['client/*.jade', 'client/views/*.jade'])
		.on('change', reloadPage)
	// styles
	gulp.watch(['client/styles/*.scss', 'client/styles/*.scss'], ['styles'])
	// scripts
	gulp.watch(['client/app.js', 'client/scripts/**/*.js'], ['lint'])

	gulp.src('./dummyfile')
    .pipe($.karma({
			configFile: 'test/client/unit/karma.conf.js',
			action: 'watch'
		}))
})

// remember to start webdriver first:
// webdriver-manager start
gulp.task('test:e2e', function() {
	var pro = $.protractor.protractor
	gulp.src(['test/client/e2e/**/*.js'])
		.pipe(pro({
			configFile: 'test/client/e2e/protractor.conf.js'
		}))
		.on('error', $.notify.onError())
})

gulp.task('test:unit', function() {
	// give gulp-karma a fake file so it's forced to read
	// from our karma.conf.js file
  return gulp.src('./dummyfile')
    .pipe($.karma({
      configFile: 'test/client/unit/karma.conf.js',
      action: 'run'
    }))
		.on('error', $.notify.onError())
})

gulp.task('test:unit', function() {
	// give gulp-karma a fake file so it's forced to read
	// from our karma.conf.js file
  return gulp.src('./dummyfile')
    .pipe($.karma({
      configFile: 'test/client/unit/karma.conf.js',
      action: 'run'
    }))
		.on('error', $.notify.onError())
})
gulp.task('watch-unit', function() {
	gulp.src('./dummyfile')
    .pipe($.karma({
			configFile: 'test/client/unit/karma.conf.js',
			action: 'watch'
		}))
})

gulp.task('test:server', function() {
  gulp.src('test/server/*Spec.js')
    .pipe($.jasmine({
      verbose: true
    }))
})
gulp.task('watch-server-unit', function() {
	gulp.watch(['server/**/*.js'], ['test:server'])
})


gulp.task('default', ['watch', 'serve'])
gulp.task('watch:e2e', ['watch', 'serve', 'test:e2e', 'watch-e2e'])
gulp.task('watch:unit', ['watch', 'serve', 'test:unit', 'watch-unit'])
gulp.task('watch:server', ['serve', 'test:server', 'watch-server-unit'])

// run build then `$ gulp replace` after to replace the proper lines in index.html
gulp.task('build', function() {
	var runSequence = require('run-sequence')
	runSequence(['styles', 'minify', 'templates'], ['clean'])
})

