'use strict';

describe('main controller', function() {

	var mainCtrl, scope;

	beforeEach(module('sohApp'));
	beforeEach(inject(function($controller, $rootScope) {
		scope = $rootScope;
		mainCtrl = $controller('MainCtrl', {
			$scope: scope
		});
	}));

	describe('MainCtrl', function() {
		it('each should be defined', function() {
			expect(scope.status).toBeDefined();
			expect(scope.test).toBeDefined();
		});

		it('should return a string of Ready', function() {
			expect(scope.status).toEqual('Ready');
		});
	});

});






