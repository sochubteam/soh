// Karma configuration
// Generated on Sat Jun 14 2014 10:15:29 GMT-0600 (MDT)


module.exports = function(config) {
  config.set({
    basePath: '../../../',
    frameworks: ['jasmine'],
    files: [
			'client/bower_components/angular/angular.js',
			'client/bower_components/angular-*/*.js',
			'client/bower_components/ngDialog/js/ngDialog.js',
			'client/*.js',
			'client/scripts/**/*.js',
			'test/client/unit/**/*.js'
		],
    exclude: [
			//'client/bower_components/**/*.min.js'
		],
		//preprocessors: {},
    reporters: ['progress'],
    port: 9876,
    colors: true,
		// possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,
		// enable watching file and executing tests whenever any file changes
    autoWatch: true,
		//browsers: ['Chrome', 'Firefox', 'Safari', 'Opera', 'IE'],
		browsers: ['PhantomJS'],
    //browsers: ['Chrome'],
		plugins: [
			'karma-chrome-launcher',
//			'karma-firefox-launcher',
//			'karma-opera-launcher',
//			'karma-ie-launcher',
			'karma-jasmine',
			'karma-phantomjs-launcher'
		],
    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  });
};
