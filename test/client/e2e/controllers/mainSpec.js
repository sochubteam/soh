'use strict';


describe('MainCtrl', function() {
	var ptor = protractor.getInstance();
	var params = browser.params;

	beforeEach(function() {
		ptor.get('/');
	});

	it('should display the correct title', function() {
		expect(ptor.getTitle()).toBe('SOH');
	});

});


