// http://www.ng-newsletter.com/posts/practical-protractor.html
exports.config = {
	
	baseUrl: 'http://localhost:1337',
  	seleniumAddress: 'http://localhost:4444/wd/hub',
  	capabilities: {
    	'browserName': 'chrome'
	},
	params: {
		baseUrl: 'http://localhost:1337'
	},
  	suites: {
	    account: 'account/*Spec.js'
	}
};
