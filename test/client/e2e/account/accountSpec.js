'use strict';

describe('User', function() {
	
	var ptor = protractor.getInstance();
	var params = browser.params;

	beforeEach(function() {
		ptor.get('/');
	});
	
	it('should be redirected to login page if not authorized', function() {
		expect(ptor.getCurrentUrl()).toBe(params.baseUrl + '/welcome');
	});
	
	it('should be redirected facebook after icon click', function() {
		var facebookAuthLink = element(by.repeater('service in registerUrls').row(0));
		//expect(facebookAuthLink.getAttribute('href')).toContain('facebook');
		ptor.sleep(3000);
	});
	
});


