'use strict';

global.__base = __dirname + '/../..';
global.__app = __base + '/client';
global.__server = __base + '/server';

process.env.NODE_ENV = process.env.NODE_ENV || 'test';

var provider = require(__server + '/modules/services/service-provider');
var config = require(__server + '/config/config')[process.env.NODE_ENV];
var mongo = require(__server + '/config/mongo')();
var redis = require(__server + '/config/redis');
var userModel = require(__server + '/models/user');

function arraysEqual(a, b) {
    var i = a.length;
    if (i != b.length) return false;
    while (i--) {
        if (a[i] !== b[i]) return false;
    }
    return true;
};

describe('Service Provider ', function() {
	
	var params, profile, user,userId;
	var mapkey = config.redis.accountsMapKey;
	
	describe('authentication method', function(){
		
		beforeEach(function(done){
			params = {
				service: 'facebook',
				code: '123456'
			};
			
			profile = {
				service: 'facebook',
				id: '12345',
				email: 'test@email.com'
			};
			
			var testUser = new userModel({
				email: profile.email,
				accounts: [profile]
			});
			
			testUser.save(function(err, createdUser){
				if(err) console.log(err); 
				user = createdUser;
				redis.hset(mapkey, 'facebook.' + profile.id, user._id, function(err){
					if(err) console.log(err);
					userId = user._id;
					done();
				});
			}); 
		});
		
		afterEach(function(done){
			user.remove(function(err){
				if(err) console.log(err);
				redis.hdel(mapkey, 'facebook.' + profile.id, function(err){
					if(err) console.log(err);
					done();
				});
			});
		});
			
		it('should return error if service name not found in request', function(done){
			delete(params.service);
			provider.authenticate(params, function(err){
				expect(err).not.toBeNull();
				expect(err).toContain('not found');
				done();
			});
		});
		
		it('should return error if service does not exists', function(done){
			params.service = 'unknown';
			provider.authenticate(params, function(err){
				expect(err).not.toBeNull();
				expect(err).toContain('not found');
				done();
			});
		});
		
		it('should find user by redis map key', function(done){
			provider._findOrCreateUser(profile, function(err, user){
				expect(err).toBeNull();
				expect(user._id).toEqual(userId);
				done(); 
			});
		});
		
		it('should find user by email address in mongo if no redis key', function(done){
			redis.hdel(mapkey, 'facebook.' + profile.id, function(err){
				expect(err).toBeNull();
				
				provider._findOrCreateUser(profile, function(err, user){
					expect(err).toBeNull();
					expect(user._id).toEqual(userId);
					expect(user.email).toEqual('test@email.com');
					done(); 
				});
			});
			
		});
		
		it('should create new user if no redis key and mongo record', function(done){
			redis.hdel(mapkey, 'facebook.' + profile.id, function(err){
				expect(err).toBeNull();
				
				user.remove(function(err){
					expect(err).toBeNull();
					
					provider._findOrCreateUser(profile, function(err, newUser){
						expect(err).toBeNull();
						expect(newUser._id).not.toBe(userId);
	
						newUser.remove(function(err){
							expect(err).toBeNull();
							done();    
						});
						
					});
				});
				
			});
			
		});
		
	});
	
	describe('getService method', function(){
		
		it('should return service object', function(){
			var service = provider.getService('facebook');
			expect(typeof(service)).toBe('object');
			expect(service.name).toBe('facebook');
		});
		
		it('should return null if service not implemented', function(){
			expect(provider.getService('notimplemented')).toBeNull();
		});
		
		it('should return null if service disabled', function(){
			provider.servicesMap['newService'] = '';
			expect(provider.getService('newService')).toBeNull();
		});
		
	});
	
	describe('parts calculation method', function(){
		
		var limits = [10, 10, 20, 30];
		var serviceAmounts = [1, 3, 5, 8];
		var answers = [[10], [4, 3, 3], [4, 4, 4, 4, 4], [4, 4, 4, 4, 4, 4, 3, 3]];
		 
		it('should calculate parts right', function(){
			for(var i = 0; i < 3; i++){
				var amount = serviceAmounts[i];
				var answer = answers[i];
				provider.limit = limits[i];
				
				var result = provider._calculateParts(amount);
				expect(arraysEqual(answer, result)).toBeTruthy();
			}
		});
		
	});
	
}); 