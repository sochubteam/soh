'use strict';

global.__base = __dirname + '/../..';

process.env.NODE_ENV = process.env.NODE_ENV || 'test';
var config = require(__base + '/server/config/config')[process.env.NODE_ENV];
var mongoose = require('mongoose');

describe('Mongo test', function() {
	
	describe('Test configuration', function(){

		it('should have "test" section', function(){
			expect(process.env.NODE_ENV).toBe('test');
			expect(config).toBeDefined();
		}); 
		
		it('should have mongo property', function(){
			expect(config.mongo).toBeDefined();
			expect(config.mongo.uri).toBeDefined();   
		});
		
		it('should have uri property inside mongo', function(){
			expect(config.mongo).toBeDefined();
			expect(config.mongo.uri).toBeDefined();   
		});
		
	});

}); 