var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	UserSchema;

UserSchema = new Schema({
	id: { type: Schema.ObjectId },
	email: { type: String, trim: true, unique: true },
	accounts: {type: Array}
});

module.exports = mongoose.model('User', UserSchema);
