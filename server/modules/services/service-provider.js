'use strict';
var async = require('async');
var config = require(__server + '/config/config')[process.env.NODE_ENV];
var user = require(__server + '/models/user');
var redis = require(__server + '/config/redis');

/**
 * ServiceProvider object contains method to authenticate
 * user via social services, retrieve and concat news feed
 */
var ServiceProvider = {
	limit: 10,
	offset: 0,
	servicesMap: {
		facebook: require('./facebook/module')
	}
};

/**
 * Proceed with response from remote service
 * to recognize or subscribe user
 * @param {Object} params
 * @param {Function} callback
 */
ServiceProvider.authenticate = function(params, callback){
	var _self = this;	
	if(!params.service){
		return callback('Service name not found in request query');
	}
	
	var service = params.service;	
	var serviceObj = _self.getService(service);
	if(!serviceObj){
		return callback('Service with name ' + service + ' not found');
	}
		
	serviceObj.getProfile(params, function(err, profile){
		if(err) return callback(err);
			
		//search for existing records
		_self._findOrCreateUser(profile, function(err, user){
			callback(err, user);
		});
	});
};

/**
 * Returns remote service module by name
 * @param {String} name
 * @return {Object}/null
 */
ServiceProvider.getService = function(name){
	var disabled = !this.servicesMap[name] || config.servicesEnabled.indexOf(name) === -1;
		
	if(disabled){
		return null;
	}else{
		return new this.servicesMap[name]();
	}
};

/**
 * Returns a list of service names with
 * subscription redirect urls
 * @return {Array}
 */
ServiceProvider.getSigninUrls = function(){
	var urls = [], _self = this;
	Object.keys(_self.servicesMap).forEach(function(key, value) {
		var service = _self.getService(key);
		urls.push({
			name: key,
			url: service.getUrl()
		});
	});
	return urls;
};

/**
 * Returns collection of user`s public posts
 * from all connected remote services
 * 
 * @todo add posts limitation logic. For example if 
 * limit equals 10 we must parse posts equally from
 * every service. If some services have no posts enough
 * shift limit to fill space with posts from another service
 * 
 * @param {String} userId mongo ObjectId
 * @param {Function} callback 
 */
ServiceProvider.getFeed = function(user, callback){
	var _self = this;
	var feed = [];
	var parts = _self._calculateParts(user.accounts.length);
	var increment = -1;
	async.concat(user.accounts, function(item, iterationCallback){
		++increment;
		var serviceName = item.service;
		var	service = _self.getService(serviceName);
						
		if(!service){
			iterationCallback('Service does not exists or disabled', []);
		}else{
			service.getFeed(item,  parts[increment], function(err, res){
				iterationCallback(err, res);
			});
		}
					
	}, function(err, result){
		if(err){ return callback(err); }
		return callback(err, result);
	});

};

/**
 * Moves feed cursor forward
 */
ServiceProvider.shiftOffset = function(){
	this.offset += this.limit;
};

/**
 * Search user in local databases by profile
 * fields from remote service
 * @param {Object} profile
 * @param {Function} callback
 */
ServiceProvider._findOrCreateUser = function(profile, callback){
	var mapkey = config.redis.accountsMapKey;
	var mapfield = profile.service + '.' + profile.id;
	
	redis.hget(mapkey, mapfield, function (err, result) {
		if(err) return callback(err, null);
		var condition = result ? {_id: result} : {email: profile.email};

		user.findOne(condition).exec(function(err, found){
			if(err){ return callback(err, null); }
			//if user not found create a new one
			if(found){ return callback(null, found); }
			
			var newUser = new user({
				email: profile.email,
				accounts: [profile]
			});
			
			newUser.save(function(err, createdUser){
				if(err){ return callback(err, null); }
				redis.hset(mapkey, mapfield, createdUser._id, function(err){
					if(err){ return callback(err, null); }
					return callback(null, createdUser);
				});
			});
		});

	});
};

ServiceProvider._calculateParts = function(servicesAmount){
	var parts = [];
	var modulo = this.limit % servicesAmount;
	var cleanLimit = this.limit - modulo;
	for(var i = 0; i < servicesAmount; i++){
		var part = cleanLimit / servicesAmount;
		if(modulo !== 0){
			++part;
			--modulo;
		}
		parts.push(part);
	}
	return parts;
};

module.exports = exports = ServiceProvider;
