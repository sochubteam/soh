'use strict';

var fb = require('fb');
var async = require('async');
var config = require('./config')[process.env.NODE_ENV];

/**
 * Constructor
 * Must have service initalization and name property
 */
function Facebook(){
	fb.options(config);
	this.name = 'facebook';
	this.service = fb;
};

/**
 * Returns service login url
 */
Facebook.prototype.getUrl = function(action){
	return this.service.getLoginUrl({scope: ['email', 'publish_actions', 'read_stream']});
};

/**
 * Returns profile requested from service
 * @param {Object} request parameters returned from remote service
 * @param {Function} callback
 * @return {Object} user profile datat from remote service
 */
Facebook.prototype.getProfile = function(params, callback){
	if(typeof(callback) !== 'function')
		throw 'Callback for module authenticate module must be a function';
		
	if(!params.code){
		callback('No token found in callback request', null);
	}
	
	var service = this.service;
	service.napi('oauth/access_token', {
		client_id:      service.options('appId'),
        client_secret:  service.options('appSecret'),
        redirect_uri:   service.options('redirectUri'),
        code:           params.code
	}, function(err, result){
		
		if(err) callback(err, null);
		
		var accessToken = result.access_token;
		var expire = result.expire;
		service.napi('/me', {
			client_id:      service.options('appId'),
        	client_secret:  service.options('appSecret'),
        	redirect_uri:   service.options('redirectUri'),
        	access_token:	accessToken
		}, function(err, profile){

			if(err){ callback(err, null); }
			
			profile.service = 'facebook';
			profile.accessToken = accessToken;
			profile.expire = expire;
			callback(null, profile);
			
		});

	});
};

/**
 * Returns array of feed
 * @param {Object} account
 * @param {Function} callback
 * @return {Array}
 */
Facebook.prototype.getFeed = function(account, limit, callback){
	
	this.service.setAccessToken(account.access_token);
	this.service.api('/'+account.id+'/home', {limit: limit, date_format: 'U'},function (result) { 
		if(!result || result.error){
			callback(result.error, []);
		}else{
			
			async.each(result.data, function(item, eachCallback){
				
				item.timestamp = item.created_time;
				eachCallback();
				
			}, function(err){
				return callback(err, result.data);
			});
	    }    
	});
};

module.exports = Facebook;