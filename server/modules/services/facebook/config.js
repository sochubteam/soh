var config = require(__server + '/config/config')[process.env.NODE_ENV];

module.exports = {
	development:{
		appId: '391316244340888',
		appSecret: 'cec1cf6c50417d313f6534a79f0c64d0',
		redirectUri: config.domain + '/auth/callback?service=facebook'
	},
	production:{},
	test:{
		appId: '391316244340888',
		appSecret: 'cec1cf6c50417d313f6534a79f0c64d0',
		redirectUri: config.domain + '/auth/callback?service=facebook'
	},
};
