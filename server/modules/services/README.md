Service Provider module v0.3
=======================

ServiceProvider module is an adapter for every social service we are going to implement.
Provider contains methods to get sorted feed from a list of services, generation sign up urls and control access tokens.

####Public properties:

* ```limit``` - amount of posts to show on main page *(default: 10)*

####Public methods:

* ```authenticate(params, callback)``` - authenticates user by request query returned from remote service after authorization. Params argument must contain name of service (ex. 'facebook') and parameters needed to get access token (for facebook - ?code=*).
* ```getService(name)``` - returns service module by name
* ```getSigninUrls()``` - returns a full list of remote services urls to authorize and accept permission request in the following format: {name: 'facebook', url: 'https://www.facebook.com/oauth/***'}
* ```getFeed(userId, callback)``` - returns a list of posts from all services connected to user account

####Protected methods:
* ```_findOrCreateUser(profile, callback)``` - tryes to find user record in redis accounts map and compare with social id. If user not found creates new one and stores new record in redis map. Profile argument presents all possible data could be received from remote service ( id, email, access_token, created timestamp etc...)

###Usage example:

```
var provider = require('/path/to/service-provider');

/**
 * Search or create new user by profile data received
 * from remote service
 */
app.get('auth/callback', function(req, res){
    provider.authenticate(req.query, function(err, user){
        req.login(user);
    });
});

var servicesSignUpUrls = provider.getSignupUrls();
/**
 * Returns service module or null if module does not exists or
 * disabled in cong (check config.enabledServices[])
 */
var facebookModule = provider.getService('facebook');

```

