'use strict';
var provider = require(__server + '/modules/services/service-provider');
var passport = require('passport');

exports.ensureAuthenticated = function(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/welcome');
};

exports.authenticate = function(req, res, next){
	provider.authenticate(req.query, function(err, user){
		if(err){
			res.send(500, err);
		}else{
			req.login(user, function(err){
				if (err) { return next(err); }
				return res.redirect('/');
			});	
		}
	});	
};
