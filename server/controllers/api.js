'use strict';

var provider = require(__server + '/modules/services/service-provider');

exports.services = function(req, res){
	res.json(provider.getSigninUrls());
};