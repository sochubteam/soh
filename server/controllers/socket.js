'use strict';

var user = require(__server + '/models/user');
var provider = require(__server + '/modules/services/service-provider');

module.exports = function(socket){
	socket.on('getFeed', function(params){
		
		if(params.limit){
			provider.limit = params.limit;
		}
		
		//TODO: just test here, make real user id parsing from
		//session
		var userId = '539f198f4fd77c714b2628bf';
		user.findById(userId, function(err, foundUser){
			if(err){ return socket.emit('feed', {error: 'User not found'}); }
			
			provider.getFeed(foundUser, function(err, feed){
				if(err){ return socket.emit('feed', {error: err}); }
				socket.emit('feed', feed);
			});
		});

	});
};
