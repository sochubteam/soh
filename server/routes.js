'use strict';

var users = require('./controllers/users');
var main = require('./controllers/index');
var	api = require('./controllers/api');

module.exports = function(app, config) {
	
	//api
	app.get('/api/services', api.services);
	
	//auth
	app.get('/auth/callback', users.authenticate);
	
	// Angular will handle client facing routes
	app.get('/views/*', main.partials);
	app.get('/', users.ensureAuthenticated, main.index);
	app.get('*', main.index);
}; 
