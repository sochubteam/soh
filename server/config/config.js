'use strict';


var path = require('path');
var rootPath = path.normalize(__dirname + '/../..');


module.exports = {
	development: {
		app: 'SOH',
		port: Number(process.env.PORT || 1337),
		domain: 'http://localhost:1337',
		mongo: {
			uri: 'mongodb://socialhub:CM5Q4sLSCuLkbPX8@ds043338.mongolab.com:43338/soh'
		},
		redis: {
			host: 'localhost',
			post: 6379,
			user: 'sohteam',
			pass: 'sohteam',
			accountsMapKey: 'accounts-map'
		},
		ip: 'localhost' || '127.0.0.1',
		root: rootPath,
		servicesEnabled: ['facebook'],
		authSuccessRoute: '/',
		authFailureRoute: '/error'
	},
	production: {
		app: 'SOH',
		domain: '',
		port: Number(process.env.PORT || 80),
		mongo: {
			uri: process.env.MONGOLAB_URI || 'mongodb://localhost/soh'
		},
		redis: {
			host: 'localhost',
			post: 6379,
			user: 'sohteam',
			pass: 'sohteam',
			accountsMapKey: 'accounts-map'
		},
		ip: process.env.IP || '0.0.0.0',
		root: rootPath,
		servicesEnabled: []
	},
	test: {
		app: 'SOH',
		port: Number(process.env.PORT || 1337),
		domain: 'http://localhost:1337',
		mongo: {
			uri: 'mongodb://localhost/soh'
		},
		redis: {
			host: 'localhost',
			post: 6379,
			user: 'sohteam',
			pass: 'sohteam',
			accountsMapKey: 'accounts-map-test'
		},
		ip: 'localhost' || '127.0.0.1',
		root: rootPath,
		servicesEnabled: ['facebook'],
		authSuccessRoute: '/',
		authFailureRoute: '/error'
	}
}
	
