'use strict';

var express = require('express');
var session = require('express-session');
var	path = require('path');
var	passport = require('passport');
var	bodyParser = require('body-parser');
var	cookieParser = require('cookie-parser');
var	morgan = require('morgan');
var env = process.env.NODE_ENV;
var config = require('./config')[env];
var User = require('./../models/user');
var LocalStrategy = require('passport-local').Strategy;
var RedisStore = require('connect-redis')(session);

module.exports = function(app){
	var config = require('./config')[env];

	if (env === 'development') {
		//app.use(morgan('dev'));
		app.use(express.static('.tmp'));
		app.use(express.static(path.join(config.root + '/client/')));
		app.set('views', config.root + '/client/');
	}

	if (env === 'production') {
		app.use(express.static(path.join(config.root + '/public')));
		app.set('views', config.root + '/views');
	}

	app.set('view engine', 'jade');
	app.use(bodyParser());
	app.use(cookieParser());
	app.use(session({
		secret: "Rc7HUWFZugVBmaUt",
		store : new RedisStore({
			host : config.redis.host,
			port : config.redis.port,
			user : config.redis.username,
			pass : config.redis.password
		}),
		cookie : {
			maxAge : 2592000000 // one month
		}
	}));
	app.use(passport.initialize());
	app.use(passport.session());

	passport.serializeUser(function(user, done) {
	  done(null, user);
	});

	passport.deserializeUser(function(obj, done) {
	  done(null, obj);
	});
};
