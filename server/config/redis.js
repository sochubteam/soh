'use strict';

var redis = require('redis');
var config = require('./config')[process.env.NODE_ENV];

var client = redis.createClient(
	config.redis.post,
	config.redis.host
)
.on('error', function (err) {
    console.log('redis error: ' + err);
})
.on('connect', function(){
	console.log('redis connected');
});

module.exports = exports = client;
