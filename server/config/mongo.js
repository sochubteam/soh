'use strict';


var mongoose = require('mongoose');
var config = require('./config')[process.env.NODE_ENV];

module.exports = function() {
	mongoose.connect(config.mongo.uri);
	mongoose.connection
		.on('open', function(){
			console.log('mongo connected: ' + config.mongo.uri);
		})
		.on('error', function(error){
			console.log('mongo error: ' + error);
		});
};
