#Branch Log
Make a TODO for your branch starting with the branch name as an h3 `###` and a list of items to accomplish using `*`, crossing them out with `~~`, any comments regarding the tasks underneath and ending your log with `***` or  `---` for separation.  
The latest branch (the branch you just created) belongs at the top.  
Only edit the branch you're currently working on, never delete a log as this Branch Log this will be used for an easy & quick overview of what was done in each branch.

###redis_service
* setup connection
* scaffold passing redis suite
* persist session
* persist cookie

---

###provider_testing
* ~~scaffold passing mongo suite~~
* ~~scaffold passing provider suite~~
***

###setup_test_env
* ~~add to package.json:~~
	* protractor
	* jasmine
	* karma
	* gulp-protractor
	* gulp-karma
* ~~setup protractor conf file~~
* ~~setup karma conf file~~
* ~~setup passing e2e test with gulp-protractor~~
* ~~setup passing unit test with gulp-karma~~
* ~~clean up gulp process- simple & with notifications~~
***

###css_structure
* ~~setup base dir with base, vars, mixins, helpers~~

create style guide base:

* ~~Color Palette~~
* ~~Typography~~
* ~~Buttons~~
* ~~Input, Checkbox & Radio~~

after base styles are agreed on and finalized,
select colors for social service themes:

* facebook
* g+
* twitter
* instagram


http://brandcolors.net/
***
