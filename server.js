'use strict';

global.__base = __dirname;
global.__app = __base + '/client';
global.__server = __base + '/server';

var express = require('express');
var app = express();
var	env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var	config = require('./server/config/config')[env];

require(__server + '/config/mongo')();
require(__server + '/config/express')(app);
require(__server + '/routes')(app);

var server = app.listen(config.port, function() {
	console.log('Express listening on port %d', config.port);
});

var io = require('socket.io').listen(server);

io.sockets.on('connection', require(__server + '/controllers/socket'));
